let mapleader = "\<Space>"

call plug#begin('~/.local/share/nvim/plugged')
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'rafi/awesome-vim-colorschemes'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'sheerun/vim-polyglot'
Plug 'vim-utils/vim-man'
Plug 'dense-analysis/ale'
" Neovim LSP Plugins
Plug 'neovim/nvim-lspconfig'
Plug 'nvim-lua/completion-nvim'
Plug 'nvim-lua/diagnostic-nvim'
Plug 'tjdevries/nlua.nvim'
Plug 'tjdevries/lsp_extensions.nvim'

" Telescope requirements...
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'

Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'tweekmonster/gofmt.vim'
Plug '9mm/vim-closer'
Plug 'tpope/vim-commentary'
Plug 'tweekmonster/startuptime.vim'

call plug#end()

" https://github.com/rafi/awesome-vim-colorschemes
syntax on
colorscheme nord
let g:airline_theme='cobalt2'
let g:ale_linters = {'javascript': ['eslint']}



" BASE Settings
set number
set relativenumber
filetype plugin indent on
set autoindent
set timeoutlen=300
set encoding=utf8
set scrolloff=2
set noshowmode
set hidden
set nowrap
set nojoinspaces
set termguicolors

" Permanent undo
set undodir=~/.vim-undo
set undofile

set noswapfile

" Use wide tabs
set shiftwidth=4
set softtabstop=4
set tabstop=4
set noexpandtab

syntax on


" Proper search
set incsearch
set ignorecase
set smartcase

set vb t_vb= " No more beeps
set ttyfast
set lazyredraw
set diffopt+=iwhite " No whitespace in vimdiff
" Make diffing better: https://vimways.org/2018/the-power-of-diff/
set diffopt+=algorithm:patience
set diffopt+=indent-heuristic

set cot=menuone,noinsert,noselect shm+=c

command! Format execute 'lua vim.lsp.buf.formatting()'

:lua << EOF
  local nvim_lsp = require('nvim_lsp')
  local on_attach = function(_, bufnr)
    require('diagnostic').on_attach()
    require('completion').on_attach()
  end
  local servers = {'jsonls', 'pyls_ms', 'vimls', 'clangd'}
  for _, lsp in ipairs(servers) do
    nvim_lsp[lsp].setup {
      on_attach = on_attach,
    }
  end
EOF

:lua <<EOF
  nvim_lsp = require "nvim_lsp"
  nvim_lsp.gopls.setup {
    cmd = {"gopls", "serve"},
    settings = {
      gopls = {
        analyses = {
          unusedparams = true,
        },
        staticcheck = true,
      },
    },
  }
EOF

:lua <<EOF
  function goimports(timeoutms)
    local context = { source = { organizeImports = true } }
    vim.validate { context = { context, "t", true } }

    local params = vim.lsp.util.make_range_params()
    params.context = context

    local method = "textDocument/codeAction"
    local resp = vim.lsp.buf_request_sync(0, method, params, timeoutms)
    if resp and resp[1] then
      local result = resp[1].result
      if result and result[1] then
        local edit = result[1].edit
        vim.lsp.util.apply_workspace_edit(edit)
      end
    end

    vim.lsp.buf.formatting()
  end
EOF

autocmd BufWritePre *.go lua goimports(1000)
" <leader><leader> toggles between buffers
nnoremap <leader><leader> <c-^>
nn <silent> <leader>n :noh<CR>

