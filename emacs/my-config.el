(setq inhibit-startup-message t)
(scroll-bar-mode -1) ; disable vertical scrollbar
(set-fringe-mode 10) ; give some padding
(tool-bar-mode -1)
(setq visible-bell t)
(menu-bar-mode -1)

(set-face-attribute 'default nil :font "Fira Code" :height 200)

;; currently not working in MELPA
;; peach-melpa.org has more modern themes
(use-package doom-themes)

(use-package command-log-mode)

;; After install this package you must run (all-the-icons-install-fonts) one time.
(use-package doom-modeline
  :init (doom-modeline-mode 1))

(use-package which-key
  :init
  (which-key-mode)
  :diminish which-key-mode
  :config (setq which-key-idle-delay 0.3))

(use-package org-bullets
  :ensure t
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(use-package ox-twbs
    :ensure t)

(use-package pdf-tools
  :ensure t)

(global-visual-line-mode 1)
(tool-bar-mode 1)
(setq ring-bell-function 'ignore)

(defalias 'yes-or-no-p 'y-or-n-p)

(setq make-backup-files nil)
(setq auto-save-default nil)
(setq create-lockfiles nil)
(setq require-final-newline t)

(defalias 'list-buffers 'ibuffer)

(windmove-default-keybindings)

(use-package ace-window
:ensure t
:init
(global-set-key [remap other-window] 'ace-window)
(custom-set-faces
'(aw-leading-char-face
((t (:inherit ace-jump-face-foreground :height 3.0)))))
)

(global-display-line-numbers-mode t)
(dolist (mode '(org-mode-hook
		term-mode-hook
		shell-mode-hook
		eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(use-package company
    :ensure t
    :diminish
    :config
    (setq company-idle-delay 0)
    (setq company-minimum-prefix-length 3)
    (add-hook 'after-init-hook 'global-company-mode))

  (use-package yasnippet
      :ensure t
      :diminish yas-minor-mode
      :config
      (add-to-list 'yas-snippet-dirs "~/.emacs.d/yasnippet-snippets")
      (add-to-list 'yas-snippet-dirs "~/.emacs.d/snippets")
      (yas-global-mode)
      (global-set-key (kbd "M-/") 'company-yasnippet))

  (use-package flycheck
    :init (global-flycheck-mode))


  (use-package web-mode
    :ensure t)

  (use-package ag
    :ensure t)

  (use-package company
    :ensure t)

  (use-package magit
    :commands (magit-status magit-get-current-branch))

  (use-package helm
    :ensure t)

  (use-package neotree
    :ensure t)

  (setq neo-smart-open t)
  (setq neo-theme 'arrow)

  (use-package emojify
    :ensure t)

  (require 'ag)
  (global-set-key (kbd "C-x C-g") 'ag-project) ;; Bind C-x C-g to ag-project

  (use-package smartparens
      :ensure t
      :diminish smartparens-mode
      :config
      (add-hook 'prog-mode-hook 'smartparens-mode))

  (use-package rainbow-delimiters
      :ensure t
      :config
      (add-hook 'prog-mode-hook 'rainbow-delimiters-mode))

    (use-package rainbow-mode
      :ensure t
      :config
      (setq rainbow-x-colors nil)
      (add-hook 'prog-mode-hook 'rainbow-mode))

  (use-package aggressive-indent
    :ensure t)

  (add-hook 'prog-mode-hook 'electric-pair-mode)

  (setq-default js-indent-level 2)

(use-package js2-mode
    :ensure t
    :mode "\\.js\\'"
    :config
    (setq-default js2-ignored-warnings '("msg.extra.trailing.comma")))
(use-package js2-refactor
    :ensure t
    :config
    (js2r-add-keybindings-with-prefix "C-c C-m")
    (add-hook 'js2-mode-hook 'js2-refactor-mode))

(use-package web-mode
    :ensure t
    :mode ("\\.html\\'")
    :config
    (setq web-mode-markup-indent-offset 2)
    (setq web-mode-engines-alist
          '(("django" . "focus/.*\\.html\\'")
            ("ctemplate" . "realtimecrm/.*\\.html\\'"))))

(use-package web-beautify
    :ensure t
    :bind (:map web-mode-map
           ("C-c b" . web-beautify-html)
           :map js2-mode-map
           ("C-c b" . web-beautify-js)))


 (use-package elm-mode
    :ensure t
    :config
    (setq elm-format-on-save t)
    (add-to-list 'company-backends 'company-elm))

(use-package irony
    :ensure t
    :hook (c-mode . irony-mode))

(use-package company-irony
    :ensure t
    :config
    (add-to-list 'company-backends 'company-irony))

(use-package js2-mode
  :ensure t)

(use-package ruby-electric
  :ensure t
  :config (add-hook 'ruby-mode-hook 'ruby-electric-mode))

(add-to-list 'auto-mode-alist
	     '("\\.\\(?:cap\\|gemspec\\|irbrc\\|gemrc\\|rake\\|rb\\|ru\\|thor\\)\\'" . ruby-mode))
(add-to-list 'auto-mode-alist
	     '("\\(?:Brewfile\\|Capfile\\|Gemfile\\(?:\\.[a-zA-Z0-9._-]+\\)?\\|[rR]akefile\\)\\'" . ruby-mode))

(use-package rvm
  :ensure t
  :config (rvm-use-default))

(use-package seeing-is-believing
  :ensure t)

(setq seeing-is-believing-prefix "C-.")
(add-hook 'ruby-mode-hook 'seeing-is-believing)
(require 'seeing-is-believing)

(use-package ruby-test-mode
  :ensure t
  :config (add-hook 'ruby-mode-hook 'ruby-test-mode))

(use-package rinari
  :ensure t)

(use-package pipenv
  :hook (python-mode . pipenv-mode)
  :init
  (setq
   pipenv-projectile-after-switch-function
   #'pipenv-projectile-after-switch-extended))

(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :init
  (setq lsp-keymap-prefix "C-c l")
  :config
  (lsp-enable-which-key-integration t))

;; Set up before-save hooks to format buffer and add/delete imports.
;; Make sure you don't have other gofmt/goimports hooks enabled.
(defun lsp-go-install-save-hooks ()
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
(add-hook 'go-mode-hook #'lsp-go-install-save-hooks)

;; Optional - provides fancier overlays.
(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode)

;; https://github.com/dominikh/go-mode.el
(use-package go-mode
  :config (add-hook 'go-mode-hook 'lsp-deferred))

;; it looks like counsel is a requirement for swiper
(use-package counsel
  :bind (("M-x" . counsel-M-x)
	 ("C-x C-f" . counsel-find-file)
	 :map minibuffer-local-map
	 ("C-r" . 'counsel-minibuffer-history))
  :config
  (setq ivy-initial-inputs-alist nil))

(use-package swiper)

(use-package ivy
  :diminish
  :bind (("C-s" . swiper)
	 :map ivy-minibuffer-map
	 ("C-l" . ivy-alt-done)
	 ("C-j" . ivy-next-line)
	 ("C-k" . ivy-previous-line)
	 :map ivy-switch-buffer-map
	 ("C-k" . ivy-previuos-line)
	 ("C-l" . ivy-done)
	 ("C-d" . ivy-switch-buffer-kill)
	 :map ivy-reverse-i-search-map
	 ("C-k" . ivy-previous-line)
	 ("C-d" . ivy-reverse-i-search-kill))
  :config
  (ivy-mode 1))

(use-package ivy-rich
  :init (ivy-rich-mode 1))

(use-package avy
:ensure t
:bind ("M-s" . avy-goto-char))

(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :custom ((projectile-completion-system 'ivy))
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  (when (file-directory-p "~/developer")
    (setq projectile-project-search-path '("~/developer")))
  (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel-projectile
  :config (counsel-projectile-mode))

 (use-package fzf
   :ensure t)

 (use-package dumb-jump
   :ensure t
   :diminish dumb-jump-mode
   :bind (("C-M-g" . dumb-jump-go)
	  ("C-M-p" . dumb-jump-back)
	  ("C-M-q" . dumb-jump-quick-look)))

; (global-set-key (kbd "M-x") #'helm-M-x)
 (global-set-key (kbd "s-f") #'helm-projectile-ag)
 (global-set-key (kbd "s-t") #'helm-projectile-find-file-dwim)

(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)

;; window resizing
(global-set-key (kbd "s-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "s-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "s-<up>") 'enlarge-window)
(global-set-key (kbd "s-<down>") 'shrink-window)

(global-set-key (kbd "C-c t") 'neotree-toggle)

(use-package dracula-theme
  :ensure t)

(load-theme 'dracula t)
